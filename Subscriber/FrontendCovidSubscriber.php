<?php

namespace SwagCovid\Subscriber;

use Enlight\Event\SubscriberInterface;

class FrontendCovidSubscriber implements SubscriberInterface
{


    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PreDispatch_Frontend_RoutingDemonstration' => 'onPreDispatchTemplateRegistration'
        ];
    }

    public function onPreDispatchTemplateRegistration(\Enlight_Event_EventArgs $args)
    {
        /** @var \Shopware_Controllers_Frontend_RoutingDemonstration $subject */
        $controller = $args->getSubject();

        //Register template directory
        $controller->View()->addTemplateDir(__DIR__. '/../ResResources/views');
    }
}
