<?php
namespace SwagCovid\Subscriber;

use Enlight\Event\SubscriberInterface;
use Shopware\Components\DependencyInjection\Container as DIContainer;
use Doctrine\DBAL\Connection;
use Shopware\Components\Theme\LessDefinition;

class Frontend implements SubscriberInterface
{
    /** @var DIContainer */
    private $container;

    /**
     * Return list of subscribed events and functions to be triggered when the events are actioned.
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'Shopware_Modules_Basket_AddArticle_Start'          => 'onBasketAddArticle',
            'Theme_Compiler_Collect_Plugin_Less'                => 'addLessFiles',
            'Enlight_Controller_Action_PreDispatch'             => 'registerTemplates'

        ];
    }



    /**
     * Add Templates to view
     *
     * @param \Enlight_Event_EventArgs $args
     */
    public function registerTemplates(\Enlight_Event_EventArgs $args)
    {
        /** @var \Enlight_Controller_Action $controller */
        $controller = $args->getSubject();
        $view       = $controller->View();
        $dir = "/var/www/html/training/custom/plugins/SwagCovid";
        $view->addTemplateDir($dir . '/Resources/views/');

    }


}
