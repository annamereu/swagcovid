<?php

class Shopware_Controllers_Frontend_SwagCovid extends Enlight_Controller_Action
{
    public function preDispatch()
    {
        $this->view->addTemplateDir(__DIR__.'/Resources/views');
        $currentAction = $this->Request()->getActionName();
        $this->view->assign('currentAction',$currentAction);

        $pluginPath = $this->container->getParameter('swag_covid.plugin_dir');
        $this->get('template')->addTemplateDir($pluginPath . '/Resources/views/');
    }
    public function indexAction()
    {
        $this->view->assign('nextPage','foo');
    }
    public function fooAction()
    {
        $this->view->assign('nextPage','index');
    }
}
