<?php

use SwagCovid\Models\Covid;

class Shopware_Controllers_Backend_SwagCovid extends \Shopware_Controllers_Backend_Application
{
    protected $model = Covid::class;
    protected $alias = 'covid';
}
