<?php

namespace SwagCovid\Models;

use Shopware\Components\Model\ModelEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="s_covid")
 */
class Covid extends ModelEntity
{
    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column()
     */
    private $name;

        /**
         * @var string $headline
         *
         * @ORM\Column()
         */
        private $headline;
        /**
         * @var string $content
         *
         * @ORM\Column(type="text", nullable=true)
         */
            private $content;
            /**
             * @var string $bgcolor
             *
             * @ORM\Column(type="text", nullable=true)
             */
                private $bgcolor;
                /**
                 * @var string $bgimg
                 *
                 * @ORM\Column(type="text", nullable=true)
                 */
                    private $bgimg;
                    /**
                     * @var string $buttontext
                     *
                     * @ORM\Column(type="text", nullable=true)
                     */
                        private $buttontext;
                        /**
                         * @var string $buttonlink
                         *
                         * @ORM\Column(type="text", nullable=true)
                         */
                            private $buttonlink;
    /**
         * @return int
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @param string $name
         */
        public function setName($name)
        {
            $this->name = $name;
        }

        /**
         * @return string
         */
        public function getName()
        {
            return $this->name;
        }
        /**
         * @param string $headline
         */
        public function setHeadline($headline)
        {
            $this->headline = $headline;
        }

        /**
         * @return string
         */
        public function getHeadline()
        {
            return $this->headline;
        }
        /**
         * @param string $content
         */
        public function setContent($content)
        {
            $this->content = $content;
        }

        /**
         * @return string
         */
        public function getContent()
        {
            return $this->content;
        }

        /**
         * @param string $bgcolor
         */
        public function setBgcolor($bgcolor)
        {
            $this->bgcolor = $bgcolor;
        }

        /**
         * @return string
         */
        public function getBgcolor()
        {
            return $this->bgcolor;
        }
        /**
         * @param string $bgimg
         */
        public function setBgimg($bgimg)
        {
            $this->bgimg = $bgimg;
        }

        /**
         * @return string
         */
        public function getBgimg()
        {
            return $this->gbimg;
        }
        /**
         * @param string $buttontext
         */
        public function setButtontext($buttontext)
        {
            $this->buttontext = $buttontext;
        }

        /**
         * @return string
         */
        public function getButtontext()
        {
            return $this->buttontext;
        }
        /**
         * @param string $buttonlink
         */
        public function setButtonlink($buttonlink)
        {
            $this->buttonlink = $buttonlink;
        }

        /**
         * @return string
         */
        public function getButtonlink()
        {
            return $this->buttonlink;
        }
      }
