
Ext.define('Shopware.apps.SwagCovid.model.Covid', {
    extend: 'Shopware.data.Model',

    configure: function() {
        return {
            controller: 'SwagCovid',
            detail: 'Shopware.apps.SwagCovid.view.detail.Covid'
        };
    },


    fields: [
        { name : 'id', type: 'int', useNull: true },
        { name : 'name', type: 'string' },
        { name : 'headline', type: 'string' },
        { name : 'content', type: 'string' },
        { name : 'bgcolor', type: 'string' },
        { name : 'bgimg', type: 'string' },
        { name : 'buttontext', type: 'string' },
        { name : 'buttonlink', type: 'string' }
    ]
});
