//

Ext.define('Shopware.apps.SwagCovid.view.list.Product', {
    extend: 'Shopware.grid.Panel',
    alias:  'widget.product-listing-grid',
    region: 'center',

    configure: function() {
        return {
            detailWindow: 'Shopware.apps.SwagCovid.view.detail.Window'
        };
    }
});
