//

Ext.define('Shopware.apps.SwagCovid.view.list.Covid', {
    extend: 'Shopware.grid.Panel',
    alias:  'widget.covid-listing-grid',
    region: 'center',

    configure: function() {
        return {
            detailWindow: 'Shopware.apps.SwagCovid.view.detail.Window'
        };
    }
});
