//

Ext.define('Shopware.apps.SwagCovid.view.detail.Covid', {
    extend: 'Shopware.model.Container',
    padding: 20,

    configure: function() {
        return {
            controller: 'SwagCovid'
        };
    }
});
