//

Ext.define('Shopware.apps.SwagCovid.view.detail.Product', {
    extend: 'Shopware.model.Container',
    padding: 20,

    configure: function() {
        return {
            controller: 'SwagCovid'
        };
    }
});