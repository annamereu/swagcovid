//

Ext.define('Shopware.apps.SwagCovid.view.detail.Window', {
    extend: 'Shopware.window.Detail',
    alias: 'widget.covid-detail-window',
    title : '{s name=title}Lists of Banners{/s}',
    height: 420,
    width: 900
});
