//

Ext.define('Shopware.apps.SwagCovid', {
    extend: 'Enlight.app.SubApplication',

    name:'Shopware.apps.SwagCovid',

    loadPath: '{url action=load}',
    bulkLoad: true,

    controllers: [ 'Main' ],

    views: [
        'list.Window',
        'list.Covid',

        'detail.Covid',
        'detail.Window'
    ],

    models: [ 'Covid' ],
    stores: [ 'Covid' ],

    launch: function() {
        return this.getController('Main').mainWindow;
    }
});
