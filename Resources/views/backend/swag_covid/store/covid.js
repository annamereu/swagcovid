//

Ext.define('Shopware.apps.SwagCovid.store.Covid', {
    extend:'Shopware.store.Listing',

    configure: function() {
        return {
            controller: 'SwagCovid'
        };
    },

    model: 'Shopware.apps.SwagCovid.model.Covid'
});
