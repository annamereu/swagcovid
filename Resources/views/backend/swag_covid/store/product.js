//

Ext.define('Shopware.apps.SwagCovid.store.Product', {
    extend:'Shopware.store.Listing',

    configure: function() {
        return {
            controller: 'SwagCovid'
        };
    },

    model: 'Shopware.apps.SwagCovid.model.Product'
});